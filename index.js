const AWS = require('aws-sdk')
const axios = require('axios')
const _ = require('lodash')
require('dotenv').config()

const { promisify } = require('util')

AWS.config.update({
    region: "eu-west-1",
    accessKeyId: process.env["ACCESS_KEY_ID"],
    secretAccessKey: process.env["SECRET_ACCESS_KEY"]
})

// This is AWS's limit, not ours
const DYNAMO_DB_BATCH_WRITE_LIMIT = 25

const client = new AWS.DynamoDB.DocumentClient()

const batchWrite = promisify(client.batchWrite)
    .bind(client)

const put = promisify(client.put)
    .bind(client)

async function writeToDynamoDbTable(tableName, list) {
    const mapped = list.map(item => ({ PutRequest: { Item: item }}))
    console.group(`Writing ${mapped.length} records to table ${tableName}`)

    let written = 0
 
    try {
        for (const chunk of _.chunk(mapped, DYNAMO_DB_BATCH_WRITE_LIMIT)) {

            await batchWrite({ RequestItems: { [tableName]: chunk } })        
            written += chunk.length
    
            console.log(`Progress: ${written}/${mapped.length}`)
        }
    }
    finally {
        console.groupEnd()
    }
}

async function getDefaultCoupons(livesite) {
    const promises = []

    for (const [tenant, tenantDetails] of Object.entries(livesite.tenant)) {
        for (const site of Object.values(tenantDetails.sites)) {
            promises.push(getDefaultCouponResponses(tenant, site.country, site.supportedLanguages))
        }
    }

    // Flatten; the result of the promises is a list of lists
    const responses = await Promise.all(promises)
    return responses.reduce((prev, curr) => [...curr, ...prev], [])
}

async function getDefaultCouponResponses(merchant, market, supportedLanguages) {
    try {
        const response = await axios.get(`https://vistaprint-org-product-catalog-product-pricing-service.products.vpsvc.com/v4/coupon/sitedefault?requestor=xavi&merchantId=${merchant}&market=${market}`)
        return supportedLanguages.map(locale => ({
            merchant,
            locale,
            couponCode: response.data.couponCode,
            details: response.data.details[locale]
        }))
    } catch (err) {
        return supportedLanguages.map(locale => ({
            merchant,
            locale,
            couponCode: null,
            details: null
        }))
    }
}

async function getLivesite() {
    return (await axios.get("https://livesite.jetstream.vpsvc.com/")).data
}

async function run() {

    const livesite = await getLivesite()

    await writeToDynamoDbTable("JimtestDefaultCoupons", await getDefaultCoupons(livesite))
}

run().catch(console.error)

